package com.eugene.yalantis_internship.first.presenter;

import com.eugene.yalantis_internship.first.contract.TaskContract;
import com.eugene.yalantis_internship.first.model.TaskRepository;

/**
 * TaskPresenter class
 * <p/>
 * Class for communicating between View(Activity) and Model(Data repository)
 */
public class TaskPresenter implements TaskContract.Presenter {

    // View instance(it can be Activity, Fragment, etc.)
    private TaskContract.View mView;

    @Override
    public void loadTaskInformation() {
        TaskRepository taskRepository = new TaskRepository(mView.getContext());
        mView.showTask(taskRepository.getExampleTask());
    }

    @Override
    public void onAttach(TaskContract.View view) {
        mView = view;
    }

    @Override
    public void onDetach() {
        mView = null;
    }
}
