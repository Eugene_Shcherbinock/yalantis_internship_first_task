package com.eugene.yalantis_internship.first.contract;

import android.content.Context;

public interface BaseView {

    Context getContext();

}
