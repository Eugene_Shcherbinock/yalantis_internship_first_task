package com.eugene.yalantis_internship.first.contract;

import com.eugene.yalantis_internship.first.model.Task;

public interface TaskContract {

    interface View extends BaseView {

        void showTask(Task task);

    }

    interface Presenter extends BasePresenter<View> {

        void loadTaskInformation();

    }

}
