package com.eugene.yalantis_internship.first.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * DateUtil class
 * <p/>
 * Class for format dates
 */
public class DateUtil {

    /**
     * Format date by default pattern
     *
     * @param date date that needs to be formated
     * @return string that holds date in pretty format
     */
    public static String format(Date date) {
        return new SimpleDateFormat(Format.NICE_FORMAT, Locale.getDefault()).format(date);
    }

    /**
     * Format date by parameter pattern
     *
     * @param date   date that needs to be formated
     * @param format format pattern
     * @return string that holds date in pretty format
     */
    public static String format(Date date, String format) {
        return new SimpleDateFormat(format, Locale.getDefault()).format(date);
    }

    /**
     * Formats holder
     */
    public abstract class Format {
        public static final String NICE_FORMAT = "dd MMMM y";
    }
}
