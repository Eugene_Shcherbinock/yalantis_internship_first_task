package com.eugene.yalantis_internship.first.contract;

public interface BasePresenter<View extends BaseView> {

    void onAttach(View view);

    void onDetach();

}
