package com.eugene.yalantis_internship.first.model;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * TaskRepository class
 * <p/>
 * Class for working with data
 */
public class TaskRepository {

    private Context mContext;

    public TaskRepository(@NonNull Context context) {
        mContext = context;
    }

    public Task getExampleTask() {
        return new Task(mContext);
    }
}
