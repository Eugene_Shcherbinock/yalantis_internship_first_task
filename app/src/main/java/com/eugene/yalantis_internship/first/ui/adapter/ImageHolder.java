package com.eugene.yalantis_internship.first.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.eugene.yalantis_internship.first.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * ImageHolder class
 * <p/>
 * Class holds ImageView for representing image of task to user
 */
public class ImageHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.image_view_task)
    ImageView mImageView;

    public ImageHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ImageView getImageView() {
        return mImageView;
    }
}
