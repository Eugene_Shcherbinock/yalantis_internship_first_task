package com.eugene.yalantis_internship.first.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.eugene.yalantis_internship.first.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * ImagesAdapter class
 * <p/>
 * RecyclerView adapter instance for inflating recycler_view_item.xml
 * and showing list of images to user
 */
public class ImagesAdapter extends RecyclerView.Adapter<ImageHolder> {

    private Context mContext;
    private List<String> mImagesUrls;

    private View.OnClickListener mImageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(mContext, v.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
        }
    };

    /**
     * Constructor with list of images urls parameter
     *
     * @param imagesUrls list of images urls
     */
    public ImagesAdapter(Context context, List<String> imagesUrls) {
        mContext = context;
        mImagesUrls = imagesUrls;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View taskImageLayout = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.recyler_view_item, parent, false);
        return new ImageHolder(taskImageLayout);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        Picasso.with(mContext)
                .load(mImagesUrls.get(position))
//                .placeholder()
//                .error()
                .resizeDimen(R.dimen.image_width, R.dimen.image_height)
                .into(holder.getImageView());
        holder.getImageView().setOnClickListener(mImageClickListener);
    }

    @Override
    public int getItemCount() {
        return mImagesUrls.size();
    }
}
