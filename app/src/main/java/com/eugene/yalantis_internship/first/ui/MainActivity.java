package com.eugene.yalantis_internship.first.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.eugene.yalantis_internship.first.R;
import com.eugene.yalantis_internship.first.contract.TaskContract;
import com.eugene.yalantis_internship.first.model.Task;
import com.eugene.yalantis_internship.first.presenter.TaskPresenter;
import com.eugene.yalantis_internship.first.ui.adapter.ImagesAdapter;
import com.eugene.yalantis_internship.first.utils.DateUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * MainActivity class
 * <p/>
 * Class for interacting with user
 * <p/>
 * Implements ITaskView interface for showing task's data like text and images
 */
public class MainActivity extends AppCompatActivity implements TaskContract.View {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.text_view_title)
    TextView mTextViewTaskTitle;

    @Bind(R.id.text_view_status)
    TextView mTextViewTaskStatus;

    @Bind(R.id.text_view_created)
    TextView mTextViewTaskCreated;

    @Bind(R.id.text_view_registered)
    TextView mTextViewTaskRegistered;

    @Bind(R.id.text_view_assigned)
    TextView mTextViewTaskAssigned;

    @Bind(R.id.text_view_responsible)
    TextView mTextViewTaskResponsible;

    @Bind(R.id.text_view_description)
    TextView mTextViewTaskDescription;

    @Bind(R.id.recycler_view_images)
    RecyclerView mRecyclerViewTaskImages;

    // TaskPresenter instance for communicating
    // with data repository and showing data to user
    private TaskContract.Presenter mTaskPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mTaskPresenter = new TaskPresenter();
        mTaskPresenter.onAttach(this);

        initializeToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mTaskPresenter.loadTaskInformation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTaskPresenter.onDetach();
        ButterKnife.unbind(this);
    }

    @OnClick({
            R.id.text_view_title, R.id.text_view_status, R.id.text_view_created,
            R.id.text_view_registered, R.id.text_view_assigned, R.id.text_view_responsible,
            R.id.text_view_description
    })
    public void showControlName(View controlView) {
        Toast.makeText(this, controlView.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Toast.makeText(this, item.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
            finish();
        }
        return true;
    }

    @Override
    // TODO: getSupportActionBar() may produce NullPointerException
    @SuppressWarnings("ConstantConditions")
    public void showTask(Task task) {
        getSupportActionBar().setTitle(task.getCode());

        mTextViewTaskTitle.setText(task.getName());
        mTextViewTaskStatus.setText(task.getStatus());
        mTextViewTaskCreated.setText(DateUtil.format(task.getCreatedDate()));
        mTextViewTaskRegistered.setText(DateUtil.format(task.getRegisteredDate()));
        mTextViewTaskAssigned.setText(DateUtil.format(task.getAssignedDate()));
        mTextViewTaskResponsible.setText(task.getResponsibleName());
        mTextViewTaskDescription.setText(task.getDescription());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL, false
        );
        mRecyclerViewTaskImages.setLayoutManager(layoutManager);
        mRecyclerViewTaskImages.setAdapter(new ImagesAdapter(this, task.getImagesUrlList()));
    }

    @Override
    public Context getContext() {
        return this;
    }

    // TODO: getSupportActionBar() may produce NullPointerException
    @SuppressWarnings("ConstantConditions")
    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
