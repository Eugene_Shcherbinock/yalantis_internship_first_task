package com.eugene.yalantis_internship.first.model;

import android.content.Context;
import android.support.annotation.NonNull;

import com.eugene.yalantis_internship.first.R;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Task class
 * <p/>
 * Class for storing all task data
 * <p/>
 * Implements Serializable to be able to put the Task class into the Bundle
 */
public class Task implements Serializable {

    private String mCode;

    private String mName;
    private String mStatus;

    private Date mCreatedDate;
    private Date mRegisteredDate;
    private Date mAssignedDate;

    private String mResponsibleName;
    private String mDescription;

    private List<String> mImagesUrlList;

    public Task(@NonNull Context context) {
        mCode = context.getString(R.string.str_task_code_example);

        mName = context.getString(R.string.str_task_name_example);
        mStatus = context.getString(R.string.str_task_status_example);

        mCreatedDate = new Date();
        mRegisteredDate = new Date();
        mAssignedDate = new Date();

        mResponsibleName = context.getString(R.string.str_task_responsible_example);
        mDescription = context.getString(R.string.str_task_description_example);

        mImagesUrlList = Arrays.asList(context.getResources().getStringArray(R.array.arr_task_images_url));
    }

    public Task(@NonNull String name, @NonNull String status,
                @NonNull Date created, @NonNull Date registered, @NonNull Date assigned,
                @NonNull String responsible, @NonNull String description,
                @NonNull List<String> imagesUrlList) {

        mName = name;
        mStatus = status;
        mCreatedDate = created;
        mRegisteredDate = registered;
        mAssignedDate = assigned;
        mResponsibleName = responsible;
        mDescription = description;
        mImagesUrlList = imagesUrlList;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Date getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(Date createdDate) {
        mCreatedDate = createdDate;
    }

    public Date getRegisteredDate() {
        return mRegisteredDate;
    }

    public void setRegisteredDate(Date registeredDate) {
        mRegisteredDate = registeredDate;
    }

    public Date getAssignedDate() {
        return mAssignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        mAssignedDate = assignedDate;
    }

    public String getResponsibleName() {
        return mResponsibleName;
    }

    public void setResponsibleName(String responsibleName) {
        mResponsibleName = responsibleName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public List<String> getImagesUrlList() {
        return mImagesUrlList;
    }

    public void setImagesUrlList(List<String> imagesUrlList) {
        mImagesUrlList = imagesUrlList;
    }
}
