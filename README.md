# Yalantis Internship Task #

Simple application with: 

* ScrollView
* TextViews 
* Horizontal RecyclerView

![dPuJbJRr_fw.jpg](https://bitbucket.org/repo/84ojgA/images/555466869-dPuJbJRr_fw.jpg)

## Requirments: ##
* Put dimens and text sizes into res folder
* Minimal SDK version = 16
* Only portrait orientation
* For image loading use one of third party libraries 
* Support different screen sizes
* Use styles 
* Back button should close application
* Ukrainian and English localization should be included.  
* When user presses any control, show toast with control name
* Project should be pushed into github.com / bitbucket
* Use google code style
* Also you should find UI mistakes, suggest how to solve it

## UI mistakes ##
![Selection_007.png](https://bitbucket.org/repo/84ojgA/images/3488893314-Selection_007.png)

1. Back button is not needed in an application with a single activity, but the task said that it should close the application
2. Too much empty space
3. Dividers and paddings between them should be the same
4. Unnecessary empty brackets